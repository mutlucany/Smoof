const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.newUser = functions.https.onRequest((req, res) => {
  var data = req.body;
  var time = admin.database.ServerValue.TIMESTAMP;
  var currentUserName;
  var userIdToken = data.userIdToken;
  var usernameRegex = /^[a-zA-Z0-9/_]{2,15}$/;
  var usernameLow = data.username.toLowerCase();
  var val =  usernameLow;
  var validUsername = val.match(usernameRegex);

    if (validUsername) {
      admin.database().ref('/nicks/'+usernameLow).once('value').then((snapshot) => {
        currentUserName = snapshot.val();
        if (currentUserName) {
          return validUsername = false;
        } else {
          return validUsername = true;
        }
      }).catch((error) => {
        console.log(error);
      });
    } else {
      validUsername = false;
    }
      if ( validUsername && !currentUserName && data.gender && data.year < 2030 || 1900 ) {
        admin.auth().verifyIdToken(userIdToken)
          .then((decodedToken) => {
            var userId = decodedToken.uid;
            var profileData = {
                registeryDate: time,
                lastCheck: time,
                username: data.username,
                fullname: data.fullname,
                gender: data.gender,
                year: data.year,
                country: data.country,
                social: "-",
                photos: "-",
                activePoll: "-",
                stats: {
                  credit: 20,
                  totalVs: 0,
                  totalSingle: 0,
                  wins: 0,
                  points: 0,
                  votesCast: 0,
                  votesRecieved: 0
                }
              };

              var nick = {
                uid: userId
              };

              var updates = {};

              updates['/users/' + userId ] = profileData;
              updates['/nicks/' + usernameLow ] = nick;

              return admin.database().ref().update(updates).then(() => {
                return res.status(200).send("SUCCESS");
              });
          }).catch((error) => {
            return res.status(401).send("Invalid ID Token");
        });
      } else {
        return res.status(400).send("Invalid User Specification(s)");

      }
    //return res.status(400).send("Unknown Error");
});

exports.endPolls = functions.https.onRequest((req, res) => {
	var data = req.body; 
	var time = admin.database.ServerValue.TIMESTAMP;
	admin.database().ref("/active-polls/").orderByChild('endTime').endAt(time).once("value").then((snapshot) => {
              snapshot.forEach((childSnapshot) => {
                var uidSecond;
                var fullnameSecond;
                var nickSecond;
                var photoSecond;
                var votesFirst;
                var votesSecond;
                var votes;
                var point;
                var seen;
                var polls;
                var pollsL;
                var pollCount;
                var voters;
                var uid = childSnapshot.val().uid;
                var pollType = childSnapshot.val().pollType;
                var pollDuration = childSnapshot.val().pollDuration;
                var fullname = childSnapshot.val().fullname;
                var nick = childSnapshot.val().nick;
                var year = childSnapshot.val().year;
                var photo = childSnapshot.val().photo;
                var votes = childSnapshot.val().results.votes;
                var point = childSnapshot.val().results.point;

                var photoTime = "-" + photo;
                var photoTimeF = parseInt(photoTime);

                admin.database().ref("/active-polls-seen/"+uid).once("value").then((snapshot) => {
        				voters = snapshot.val();
        				if (pollType == "single") {
                  admin.database().ref("/users/"+uid+"/stats").once("value").then((snapshot) => {
                    var data = snapshot.val();
                    var votesFinal = votes + data.votesRecieved;
                    var totalSingle = data.totalSingle + 1;
                    var pointFinal = (data.points * data.totalSingle + point ) / totalSingle;

                    admin.database().ref("/users/"+uid+"/stats").update({
                      points : pointFinal,
                      totalSingle: totalSingle,
                      votesRecieved: votesFinal
                    });

                    admin.database().ref("/users/"+uid).update({
                      activePoll: "-",
                    });



                    admin.database().ref("/users/"+uid+"/photos/"+photo).update({
                      point: point,
                      votes: votes,
                      pollType: "single",
                      voters: voters,
                      time: photoTimeF
                    });

                    var newNotifyRef = admin.database().ref("/notifications/" + uid).push();
                    var newNotifyKey = newNotifyRef.key;
                    newNotifyRef.set({
                      type: "normal",
                      key: newNotifyKey,
                      msgID: "pollEnded"
                    });
                  });

                } else {
                  uid = childSnapshot.val().uid;
                  pollType = childSnapshot.val().pollType;
                  pollDuration = childSnapshot.val().pollDuration;
                  fullname = childSnapshot.val().fullname;
                  nick = childSnapshot.val().nick;
                  year = childSnapshot.val().year;
                  photo = childSnapshot.val().photo;
                  uidSecond = childSnapshot.val().uidSecond;
                  fullnameSecond = childSnapshot.val().fullnameSecond;
                  nickSecond = childSnapshot.val().nickSecond;
                  yearSecond = childSnapshot.val().yearSecond;
                  photoSecond = childSnapshot.val().photoSecond;
                  votesFirst = childSnapshot.val().results.votesFirst;
                  votesSecond = childSnapshot.val().results.votesSecond;
                  var totalVotes = votesFirst + votesSecond;

                  admin.database().ref("/users/"+uid+"/stats").once("value").then((snapshot) => {
                    var data = snapshot.val();
                    var votesFinal = votesFirst + data.votesRecieved;
                    var totalVs = data.totalVs + 1;
                    var percent = Math.round(votesFirst / (votesFirst + votesSecond)*100);
                    var percentText = Math.round(percent) + "%";
                    var wins;

                    if ( votesFirst > votesSecond || votesFirst == votesSecond) {
                      wins = data.wins + 1;
                    } else {
                      wins = data.wins;
                    }


                    admin.database().ref("/users/"+uid+"/stats").update({
                      totalVs: totalVs,
                      wins: wins,
                      votesRecieved: votesFinal
                    });

                    admin.database().ref("/users/"+uid).update({
                      activePoll: "-",
                    });

                    admin.database().ref("/users/"+uid+"/photos/"+photo).update({
                      point: percentText,
                      votes: votesFinal,
                      pollType: "vs",
                      opponentPhoto: photoSecond,
                      opponentUid: uidSecond,
                      opponentNick: nickSecond,
                      voters: voters,
                      time: photoTimeF
                    });
                    var newNotifyRef = admin.database().ref("/notifications/" + uid).push();
                    var newNotifyKey = newNotifyRef.key;
                    newNotifyRef.set({
                      type: "normal",
                      key: newNotifyKey,
                      msgID: "pollEnded"
                    });
                  });

                  admin.database().ref("/users/"+uidSecond+"/stats").once("value").then((snapshot) => {
                    var data = snapshot.val();
                    var votesFinal = votesSecond + data.votesRecieved;
                    var totalVs = data.totalVs + 1;
                    var percent = Math.round(votesSecond / (votesFirst + votesSecond)* 100);
                    var percentText = Math.round(percent) + "%";
                    var wins;

                    if ( votesSecond > votesFirst || votesFirst == votesSecond) {
                      wins = data.wins + 1;
                    } else {
                      wins = data.wins;
                    }

                    admin.database().ref("/users/"+uidSecond+"/stats").update({
                      totalVs: totalVs,
                      wins: wins,
                      votesRecieved: votesFinal
                    });

                    admin.database().ref("/users/"+uidSecond).update({
                      activePoll: "-",
                    });

                    admin.database().ref("/users/"+uidSecond+"/photos/"+photoSecond).update({
                      point: percentText,
                      votes: votesFinal,
                      pollType: "vs",
                      opponentPhoto: photo,
                      opponentUid: uid,
                      opponentNick: nick,
                      voters: voters,
                      time: photoTimeF
                    });

                    var newNotifyRef = admin.database().ref("/notifications/" + uidSecond).push();
                    var newNotifyKey = newNotifyRef.key;
                    newNotifyRef.set({
                      type: "normal",
                      key: newNotifyKey,
                      msgID: "pollEnded"
                    });

                  });
                }
                admin.database().ref("/active-polls/"+uid).remove();
                admin.database().ref("/active-polls-seen/"+uid).remove();

                });
              });
            });
            return res.status(200).send("SUCCESS");
});

exports.choiceOne = functions.https.onRequest((req, res) => {
		  var data = req.body;
          var pollID = data.pollID;
          var userIdToken = data.userIdToken;
          var votesFirst;
          var votesSecond;
          var previousVote;
          admin.auth().verifyIdToken(userIdToken)
          .then((decodedToken) => {
          var userId = decodedToken.uid;
          admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then((snapshot) => {
            var value = snapshot.val();
            if (value) {
              if ( value.rate == "second" ) {
                admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                  if ( data !== null ) {
                    votesFirst = data.votesFirst;
                    votesSecond = data.votesSecond;

                    finalFirst = votesFirst + 1;
                    finalSecond = votesSecond - 1;

                    
                    return { votesFirst: finalFirst, votesSecond: finalSecond };
                  } else {
                    return 0;
                  }
                }, (error, committed, snapshot) => {
                  if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                    time = admin.database.ServerValue.TIMESTAMP;

                    var voteTime = "-"+time;
                    var voteTimeP = parseInt(voteTime);
                    admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                      time: voteTimeP,
                      rate: "first",
                      uid: userId
                    });

                    admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
          						var nick = snapshot.val().nick;
          						var uid = snapshot.val().uid;
          						var photo = snapshot.val().photo;
          						var nickSecond = snapshot.val().nickSecond;
          						var uidSecond = snapshot.val().uidSecond;
          						var photoSecond = snapshot.val().photoSecond;
                      var voteTime = "-"+time;
                      var voteTimeP = parseInt(voteTime);

          						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                        time: voteTimeP,
                        rate: "first",
                        pollType: "vs",
                        uid: uid,
                        nick: nick,
                        photo: photo,
                        uidSecond: uidSecond,
                        nickSecond: nickSecond,
                        photoSecond: photoSecond
                      });
          					});
                  }
                }, true);
              }
            } else {
              admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                if ( data !== null ) {
                  votesFirst = data.votesFirst;
                  votesSecond = data.votesSecond;

                  finalFirst = votesFirst + 1;
                  finalSecond = votesSecond ;

                  
                  return { votesFirst: finalFirst, votesSecond: finalSecond };
                } else {
                  return 0;
                }
              }, (error, committed, snapshot) => {
                if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                  time = admin.database.ServerValue.TIMESTAMP;

                  var voteTime = "-"+time;
                  var voteTimeP = parseInt(voteTime);

                  admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                    time: voteTimeP,
                    rate: "first",
                    uid: userId
                  });

                  admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
						var nick = snapshot.val().nick;
						var uid = snapshot.val().uid;
						var photo = snapshot.val().photo;
						var nickSecond = snapshot.val().nickSecond;
						var uidSecond = snapshot.val().uidSecond;
						var photoSecond = snapshot.val().photoSecond;
            var voteTime = "-"+time;
            var voteTimeP = parseInt(voteTime);
						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                          time: voteTimeP,
                          rate: "first",
                          pollType: "vs",
                          uid: uid,
                          nick: nick,
                          photo: photo,
                          uidSecond: uidSecond,
                          nickSecond: nickSecond,
                          photoSecond: photoSecond
                        });
					  });

                  admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
                    if ( votesCast !== null ) {
                      return votesCast + 1 ;
                    } else {
                      return 0;
                    }
                  });
                }
              }, true);
            }
        return res.status(200).send("SUCCESS");
});

exports.choiceTwo = functions.https.onRequest((req, res) => {
		  var data = req.body;
          var pollID = data.pollID;
          var userIdToken = data.userIdToken;
          var votesFirst;
          var votesSecond;
          var previousVote;
          admin.auth().verifyIdToken(userIdToken)
          .then((decodedToken) => {
          var userId = decodedToken.uid;
          admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then( (snapshot) => {
            var value = snapshot.val();
            if (value) {
              if ( value.rate == "first" ) {
                admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                  if ( data !== null ) {
                    votesFirst = data.votesFirst;
                    votesSecond = data.votesSecond;

                    finalFirst = votesFirst - 1;
                    finalSecond = votesSecond + 1;

                    return { votesFirst: finalFirst, votesSecond: finalSecond };
                  } else {
                    return 0;
                  }
                }, (error, committed, snapshot) => {
                  if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                    time = admin.database.ServerValue.TIMESTAMP;

                    var voteTime = "-"+time;
                    var voteTimeP = parseInt(voteTime);

                    admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                      time: voteTimeP,
                      rate: "second",
                      uid: userId
                    });

                    admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
						var nick = snapshot.val().nick;
						var uid = snapshot.val().uid;
						var photo = snapshot.val().photo;
						var nickSecond = snapshot.val().nickSecond;
						var uidSecond = snapshot.val().uidSecond;
						var photoSecond = snapshot.val().photoSecond;

            var voteTime = "-"+time;
            var voteTimeP = parseInt(voteTime);
						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                          time: voteTimeP,
                          rate: "second",
                          pollType: "vs",
                          uid: uid,
                          nick: nick,
                          photo: photo,
                          uidSecond: uidSecond,
                          nickSecond: nickSecond,
                          photoSecond: photoSecond
                        });
					  });
                  }
                }, true);
              }
            } else {
              admin.database().ref("/active-polls/"+pollID+"/results/").transaction((data) => {
                if ( data !== null ) {
                  votesFirst = data.votesFirst;
                  votesSecond = data.votesSecond;

                  finalFirst = votesFirst;
                  finalSecond = votesSecond + 1 ;

                  
                  return { votesFirst: finalFirst, votesSecond: finalSecond };
                } else {
                  return 0;
                }
              }, (error, committed, snapshot) => {
                if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                  time = admin.database.ServerValue.TIMESTAMP;

                  var voteTime = "-"+time;
                  var voteTimeP = parseInt(voteTime);

                  admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                    time: voteTimeP,
                    rate: "second",
                    uid: userId
                  });
				 admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
						var nick = snapshot.val().nick;
						var uid = snapshot.val().uid;
						var photo = snapshot.val().photo;
						var nickSecond = snapshot.val().nickSecond;
						var uidSecond = snapshot.val().uidSecond;
						var photoSecond = snapshot.val().photoSecond;

            var voteTime = "-"+time;
            var voteTimeP = parseInt(voteTime);

						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                          time: voteTimeP,
                          rate: "second",
                          pollType: "vs",
                          uid: uid,
                          nick: nick,
                          photo: photo,
                          uidSecond: uidSecond,
                          nickSecond: nickSecond,
                          photoSecond: photoSecond
                        });
					  });
                  admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
                    if ( votesCast !== null ) {
                      return votesCast + 1 ;
                    } else {
                      return 0;
                    }
                  });
                }
              }, true);
            }
          });
         });
	return res.status(200).send("SUCCESS");
});

exports.rate = functions.https.onRequest((req, res) => {
	var data = req.body;
	var rate = data.rate;   
	var userIdToken = data.userIdToken;
  var pollID = data.pollID;
  var point;
  var votes;
  var finalPoint;
  var pointChange;
              
			admin.auth().verifyIdToken(userIdToken)
        	  .then((decodedToken) => {
         		 var userId = decodedToken.uid;
              admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then( (snapshot) => {
                var value = snapshot.val();
                if (value) {
                  admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                    if ( data !== null ) {
                      point = data.point;
                      votes = data.votes;

                      pointChange = (rate - value.rate ) / votes;
                      finalPoint = point + pointChange;

                      $("#"+pollID+" .result p").html(Math.round(finalPoint)+"<span>/10</span>");
                      return { point: finalPoint, votes: votes };
                    } else {
                      return 0;
                    }

                  }, (error, committed, snapshot) => {
                    if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                      time = admin.database.ServerValue.TIMESTAMP;
                      var voteTime = "-"+time;
                      var voteTimeP = parseInt(voteTime);
                      admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                        time: voteTimeP,
                        rate: rate,
                        uid: userId
                      });

                      admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
            						var nick = snapshot.val().nick;
            						var photo = snapshot.val().photo;
                        var voteTime = "-"+time;
                        var voteTimeP = parseInt(voteTime);

            						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                            time: voteTimeP,
                            rate: rate,
                            pollType: "single",
                            uid: pollID,
                            nick: nick,
                            photo: photo,

                          });
            					  });

                    }
                  }, true);

                } else {
                  time = admin.database.ServerValue.TIMESTAMP;
                  admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                    if ( data !== null ) {
                      point = data.point;
                      votes = data.votes;

                      finalPoint = (point * votes + rate)/(votes + 1 );

                      $("#"+pollID+" .result p").html(Math.round(finalPoint)+"<span>/10</span>");
                      return { point: finalPoint, votes: votes +1 };
                    } else {
                      return 0;
                    }

                  }, (error, committed, snapshot) => {
                    if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                      var voteTime = "-"+time;
                      var voteTimeP = parseInt(voteTime);
                      admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                       time: voteTimeP,
                        rate: rate,
                        uid: userId,
                      });

                      admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
          						var nick = snapshot.val().nick;
          						var photo = snapshot.val().photo;
                      var voteTime = "-"+time;
                      var voteTimeP = parseInt(voteTime);

          						admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                                    time: voteTimeP,
                                    rate: rate,
                                    pollType: "single",
                                    uid: pollID,
                                    nick: nick,
                                    photo: photo,

                                  });
          					  });

                      admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
                        if ( votesCast !== null ) {
                          return votesCast + 1 ;
                        } else {
                          return 0;
                        }
                      });
                    }
                  }, true);
                }
              });
            });
          });
         });
	return res.status(200).send("SUCCESS");
});



exports.nick = functions.https.onRequest((req, res) => {
	var data = req.body;
	var userIdToken = data.userIdToken;
    var newUsername = data.newUsername;
    var newUsernameLow = newUsername.toLowerCase();
              var validUsername = newUsername.match(usernameRegex);
              if (validUsername) {
                admin.database().ref('/nicks/'+newUsernameLow).once('value').then((snapshot) => {
                  var currentUserName = snapshot.val();
                  if ( currentUserName ) {
                   return res.status(400).send("Username already exists.");
                  } else {
                   admin.auth().verifyIdToken(userIdToken)
        	  .then((decodedToken) => {
         		 var userId = decodedToken.uid;
                try {
                  admin.database().ref('/users/'+userId).once('value').then((snapshot) => {
                    var oldUsername = snapshot.val().username;
                    var oldUsernameLow = oldUsername.toLowerCase();
                    var activePoll = snapshot.val().activePoll;
                    admin.database().ref("nicks/" + oldUsernameLow).remove();

                    var nick = {
                      uid: userId
                    };

                    var updates = {};

                    updates['/users/' + userId + '/username' ] = newUsername;
                    updates['/nicks/' + newUsernameLow ] = nick;

                    if (activePoll !== "-") {
                      admin.database().ref('/pending-polls/'+activePoll).once('value').then((snapshot) => {
                        try {
                          var pollType = snapshot.val().type;
                          if(pollType){
                            if (pollType == "single") {
                              admin.database().ref("/pending-polls/" + activePoll).update({
                                username: newUsername
                              });
                            } else {
                              var pollUID = snapshot.val().uid;
                              if (pollUID == userId ) {
                                admin.database().ref("/pending-polls/" + activePoll).update({
                                  username: newUsername
                                });
                              } else {
                                admin.database().ref("/pending-polls/" + activePoll).update({
                                  usernameSecond: newUsername
                                });
                              }
                            }
                          }

                        } catch (e){
                          admin.database().ref('/active-polls/'+activePoll).once('value').then((snapshot) => {
                            try {
                              var pollType = snapshot.val().pollType;
                              if (pollType){
                                if (pollType == "single") {
                                  admin.database().ref("/active-polls/" + activePoll).update({
                                    nick: newUsername
                                  });
                                } else {
                                  var pollUID = snapshot.val().uid;
                                  if (pollUID == userId ) {
                                    admin.database().ref("/active-polls/" + activePoll).update({
                                      nick: newUsername
                                    });
                                  } else {
                                    admin.database().ref("/active-polls/" + activePoll).update({
                                      nickSecond: newUsername
                                    });
                                  }
                                }
                              }
                            } catch (e) {
								return res.status(400).send(e);
                            }
                          });
                        }
                      });
                    }

                    admin.database().ref().update(updates).then( () => {
                      return res.status(200).send("Saved");
                    });
                  });

                } catch (error) {
                  return res.status(400).send(error);
                }
           });
                  }
                });
              } else {
                return res.status(400).send("Username already exists.");
              }
    
});






exports.nick = functions.https.onRequest((req, res) => {
var data = req.body;
var ref = data.ref
                       var pollType = data.pollType;
                       var toWhom = data.toWhom;
                       var pollDuration = data.pollDuration;
                       var userIdToken = data.userIdToken;
                       admin.auth().verifyIdToken(userIdToken)
        	  .then((decodedToken) => {
         		 var userId = decodedToken.uid;

                             
                       });
                             
                         });