$(document).ready(function(){
var pollType ;
var pollDuration;
var toWhom;
var uid ;
var fullname;
var username;
var year;
var gender;
var photo;
var lastCheck;
var uidSecond;
var fullnameSecond;
var usernameSecond;
var yearSecond;
var genderSecond;
var photoSecond;
var ageRange;

function acceptPoll() {

var time = window.parent.firebaseRef.database.ServerValue.TIMESTAMP;
  var now;
  var endTime;
  var newNotifyKey;
  var newNotifyRef;
  window.parent.firebaseRef.database().ref("/pending-polls/"+uid).update({
    lastCheck: time
  }).then(function () {
    window.parent.firebaseRef.database().ref('/pending-polls/'+uid).once('value').then(function(snapshot) {
      now = snapshot.val().lastCheck;
      if (pollDuration == "5m") {
        endTime = now + 300000;
      } else if (pollDuration == "1h") {
        endTime = now + 3600000;
      } else if (pollDuration == "12h") {
        endTime = now + 43200000;
      } else {
        endTime = now + 86400000;
      }

      window.parent.firebaseRef.database().ref("/active-polls/"+uid).update({
        pollType : pollType,
        pollDuration : pollDuration,
        uid : uid,
        fullname : fullname,
        nick : username,
        year : year,
        gender : gender,
        photo : photo,
        endTime : endTime,
        results: "-"
      });

      window.parent.firebaseRef.database().ref("/active-polls-seen/"+uid+"/"+uid).update({
        time: time
      });




      if ( pollType == "vs" ) {
        window.parent.firebaseRef.database().ref("/active-polls/"+uid).update({
          uidSecond : uidSecond,
          fullnameSecond : fullnameSecond,
          nickSecond : usernameSecond,
          yearSecond : yearSecond,
          genderSecond : genderSecond,
          photoSecond : photoSecond,
        });

        window.parent.firebaseRef.database().ref("/active-polls-seen/"+uid+"/"+uidSecond).update({
          time: time
        });

        window.parent.firebaseRef.database().ref("/active-polls/"+uid+"/results").update({
          votesFirst: 0,
          votesSecond: 0
        });
        window.parent.firebaseRef.database().ref("/pending-polls/"+uidSecond).remove();
        window.parent.firebaseRef.database().ref("/users/"+uidSecond).update({
          activePoll: uid
        });

        window.parent.firebaseRef.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange +"/"+ uidSecond).remove();

        newNotifyRef = window.parent.firebaseRef.database().ref("/notifications/" + uid).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });

        newNotifyRef = window.parent.firebaseRef.database().ref("/notifications/" + uidSecond).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });
      } else {
        window.parent.firebaseRef.database().ref("/active-polls/"+uid+"/results").update({
          point: 0,
          votes: 0
        });

        newNotifyRef = window.parent.firebaseRef.database().ref("/notifications/" + uid).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });

      }

      window.parent.firebaseRef.database().ref("/pending-polls/"+uid).remove();
      window.parent.firebaseRef.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange +"/"+ uid).remove();
      window.parent.firebaseRef.database().ref("/users/"+uid).update({
        activePoll: uid
      });


      getPoll();
    });
  });
}

function getPoll() {

  window.parent.firebaseRef.database().ref("/pending-polls/").once("value").then(function(snapshot) {
    var pending = snapshot.val();
    if (pending) {
      var pendingL = Object.keys(pending).length;

    }
  });

  pollType = undefined ;
  pollDuration = undefined;
  uid = undefined ;
  fullname = undefined;
  username = undefined;
  year = undefined;
  gender = undefined;
  photo = undefined;
  lastCheck = undefined;
  uidSecond = undefined;
  fullnameSecond = undefined;
  usernameSecond = undefined;
  yearSecond = undefined;
  genderSecond = undefined;
  photoSecond = undefined;
  ageRange = undefined;

  window.parent.firebaseRef.database().ref("/pending-polls/").orderByChild('lastCheck').limitToFirst(1).once("value").then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      pollType = childSnapshot.val().type;
      pollDuration = childSnapshot.val().pollDuration;
      uid = childSnapshot.val().uid;
      fullname = childSnapshot.val().fullname;
      username = childSnapshot.val().username;
      year = childSnapshot.val().year;
      gender = childSnapshot.val().gender;
      photo = childSnapshot.val().photo;
      lastCheck = childSnapshot.val().lastCheck;
      toWhom = childSnapshot.val().toWhom;



			//AUTO ACCEPT


      if ( pollType == "vs") {


        if (toWhom == "determined") {
          uidSecond = childSnapshot.val().uidSecond;
          fullnameSecond = childSnapshot.val().fullnameSecond;
          usernameSecond = childSnapshot.val().usernameSecond;
          yearSecond = childSnapshot.val().yearSecond;
          genderSecond = childSnapshot.val().genderSecond;
          photoSecond = childSnapshot.val().photoSecond;
					setTimeout(function(){
						acceptPoll();
					},1000);
        } else {
          ageRange = childSnapshot.val().ageRange;
          try {
            window.parent.firebaseRef.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange).limitToFirst(2).once("value").then(function(snapshot) {
              var pending = snapshot.val();
              var pendingL = Object.keys(pending).length;
              if (pendingL !== 1) {
                snapshot.forEach(function(childSnapshot) {
                  uidSecond = childSnapshot.val().uid;
                  if ( childSnapshot.val().uid !== uid ) {

                    uidSecond = childSnapshot.val().uid;
                    window.parent.firebaseRef.database().ref('/pending-polls/'+uidSecond).once('value').then(function(snapshot) {
                      fullnameSecond = snapshot.val().fullname;
                      usernameSecond = snapshot.val().username;
                      yearSecond = snapshot.val().year;
                      genderSecond = snapshot.val().gender;
                      photoSecond = snapshot.val().photo;
                      uidSecond = childSnapshot.val().uid;
											setTimeout(function(){
												acceptPoll();
											},1000);
                    });
                  }

                });
              } else {
                var time = window.parent.firebaseRef.database.ServerValue.TIMESTAMP;
                window.parent.firebaseRef.database().ref("/pending-polls/"+uid).update({
                  lastCheck: time
                });

                if (pendingL !== 1) {
                  getPoll();
                }

              }
            });

          } catch (e) {
            var time = window.parent.firebaseRef.database.ServerValue.TIMESTAMP;
            window.parent.firebaseRef.database().ref("/pending-polls/"+uid).update({
              lastCheck: time
            });
            Materialize.toast("Error while looking for match: "+e, 4000);
            //getPoll();
          }

        }
      } else {
				setTimeout(function(){
					acceptPoll();
				},1000);
      }
    });
  });
}

getPoll();

$("#accept").click(function () {


});


});
