$(document).ready(function(){

  //Is it mobile device?
  function isItMobile() {
    return $( "#isitmobile" ).css("height");
    // 0px = no
    // 1px = yes
  }

  var mobile = isItMobile();

  //Search
  $( "#search" ).click( function () {
    $( "input[name='search']" ).velocity({ opacity: 1, left: 0, }, 100);
    $( "input[name='search']" ).css("display","block");
    document.getElementById("search-input").focus();
    $("#search-input").select();
  });

  $(document).on('mousedown', function(event) {
    if (!$(event.target).closest( "input[name='search'], #search" ).length) {
      $( "input[name='search']" ).velocity({ opacity: 0, left: 10 }, 200);
      $('.dropdown-button').dropdown('close');
      setTimeout(function () {
          $( "input[name='search']" ).css("display","none");
      },200);
    }
  });

  $(window).blur( function(e){
    $( "input[name='search']" ).velocity({ opacity: 0, left: 10 }, 200);
    $('.dropdown-button').dropdown('close');
       setTimeout(function () {
          $( "input[name='search']" ).css("display","none");
      },200);
  });

  $(document).on("submit", "#search-box", function (e) {
    var hash = $("input[name='search']").val();
    firebaseRef.database().ref("/nicks/"+hash).once("value").then(function(snapshot) {
      try {
        var getUID = snapshot.val().uid;
        window.urlUser = getUID;
        window.parent.fullbrowse();
        document.getElementById('fullbrowse').contentWindow.location.replace("profile2.html");
      } catch (e) {
        Materialize.toast(noUserMsg, 5000);
      }
    });

    e.preventDefault();
  });

  //Tab change
  $( document ).on("click", "#navbar1 > .navicon_container >.navicon", function () {
    $( ".navicon" ).removeClass("navicon-active");
    $( this ).addClass("navicon-active");

    var offset = $( this ).offset();
    setTimeout(function () {
      if ( mobile == "0px" ) {
        $( ".indicator" ).velocity({ left: offset.left - 18 },300);
      } else {
        $( ".indicator" ).velocity({ left: offset.left - 11 },300);
      }

    },200);


    var section = $( this ).attr("id");
    if ( section == "home") {
      document.getElementById('s-home').contentWindow.location.replace("home.html");

      setTimeout(function () {
        $( "#sections" ).velocity({ left: 0 },300);

      },200);
      setTimeout(function () {
        document.getElementById('s-upload').contentWindow.location.replace("about:blank");
        document.getElementById('s-profile').contentWindow.location.replace("about:blank");
      },550);

    } else if ( section == "upload" ) {
      document.getElementById('s-upload').contentWindow.location.replace("upload.html");

      setTimeout(function () {
        $( "#sections" ).velocity({ left: "-100vw" },300);

      },200);
      setTimeout(function () {
        document.getElementById('s-profile').contentWindow.location.replace("about:blank");
        document.getElementById('s-home').contentWindow.location.replace("about:blank");
      },550);

    } else {
      document.getElementById('s-profile').contentWindow.location.replace("profile.html");
      setTimeout(function () {
        $( "#sections" ).velocity({ left: "-202vw" },300);

      },200);

      setTimeout(function () {
        document.getElementById('s-upload').contentWindow.location.replace("about:blank");
        document.getElementById('s-home').contentWindow.location.replace("about:blank");

      },550);

    }
  });



  // Toggle Fullscreen Page
  window.fullbrowse = function(a) {
    if ( a == "hide" ) {
      $("#fullbrowse")
        .velocity({ opacity: 0, top: "30vh" },200)
        .velocity({ top: "100vh" },0);
        setTimeout(function () {
          $("#fullbrowse").hide();
        },200);
    } else {
      setTimeout(function () {
        $("#fullbrowse").show();
        $("#fullbrowse")
          .velocity({ top: "30vh" }, 0)
          .velocity({ opacity: 1, top: "0vh" },200);
      },250);

    }
  };

  window.fullbrowse2 = function(a) {
    if ( a == "hide" ) {
      $("#fullbrowse2")
        .velocity({ opacity: 0, top: "30vh" },200)
        .velocity({ top: "100vh" },0);
        setTimeout(function () {
          $("#fullbrowse2").hide();
        },200);
    } else {
      setTimeout(function () {
        $("#fullbrowse2").show();
        $("#fullbrowse2")
          .velocity({ top: "30vh" }, 0)
          .velocity({ opacity: 1, top: "0vh" },200);
      },250);

    }
  };

  window.removeHash = function () {
    var scrollV, scrollH, loc = window.location;
    if ("pushState" in history)
        history.pushState("", document.title, loc.pathname + loc.search);
    else {
        // Prevent scrolling by storing the page's current scroll offset
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;

        loc.hash = "";

        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
  };

  //Close Any fullbrowse
  window.parent.closeFB = function() {
    fullbrowse("hide");
    fullbrowse2("hide");
     setTimeout(function () {
      removeHash();
      document.getElementById('fullbrowse').contentWindow.location.replace("about:blank");
     },200);
  };

  window.parent.closeFB2 = function() {
    fullbrowse2("hide");
     setTimeout(function () {

      document.getElementById('fullbrowse2').contentWindow.location.replace("about:blank");
     },200);
  };

  //Load Welcome
  setTimeout(function () {
    document.getElementById('fullbrowse').contentWindow.location.replace("welcome.html");
  },50);


  //Open Settings
  $("#settings").click(function () {
    location.hash = "settings";
  });


  //Open Feedback
  $("#feedback").click(function () {
    location.hash = "feedback";
  });

  //Cordova back button behavior
  document.addEventListener("backbutton", onBackKeyDown, false);

  function onBackKeyDown() {
    if($('#fullbrowse').css('display') == 'none' && $('#fullbrowse2').css('display') == 'none') {
      navigator.app.exitApp();
    } else {
      if( $('#fullbrowse2').css('display') == 'none' ) {
        closeFB();
	  } else {
		closeFB2();
	  }

    }
  }

});
