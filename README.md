# Smoof
Discontinued photo-sharing app project from 2015. The main focus was sharing selfies and expecting likes from the whole world instead of friends.
It is still live at <https://smoof-5d949.firebaseapp.com/>. Some functionality won't work since Firebase Functions is not active.

## Features
- One can share their photo stand-alone to be rated by random people on a scale of 1 to 10.
- One can share their photo to be matched with certain users or random users similar to their gender (>_<) and age to compete with each other.
- No friend or chat mechanism to facilitate anonymity.

## Why discontinued
It didn't feel ethical. The main idea was to compete in two different selfies. To make it meaningful we didn't want people to vote for a photo because of age or gender difference. Choosing between an elderly and a teen or a girl and a boy would be bad. We try to match people like Tinder. When we're trying to achieve this, things get even more complicated. 

People can be matched with similar ages. This *might* be fine. When it comes to gender. Well, it is not binary while in our app it is.

Another reason, we realized is that people want likes to be seen by people they know. 

The idea felt good back in 2015. I did all the coding myself also I was in early stages of coding knowledge. So, it took a few years to create a somewhat working product.

## The Code
Firebase (Authentication, Realtime Database, Storage, Functions) is used for back-end stuff. 
Be aware of jQuery and spaghetti. I was early stages of Javascript while I was working on this project.

## Screenshots
![Screen recording of general usage of the app](screenrecording.gif)
![Screenshot of votes](screenshot1.png)
![Screenshot of the profile](screenshot2.png)
![Screenshot of the upload dialog](screenshot3.png)
