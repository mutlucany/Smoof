const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.newUser = functions.https.onRequest((req, res) => {
  var data = req.body;
  var time = Date.now();
  var currentUserName;
  var userIdToken = data.userIdToken;
  var usernameRegex = /^[a-zA-Z0-9/_]{2,15}$/;
  var usernameLow = data.username.toLowerCase();
  var val =  usernameLow;
  var validUsername = val.match(usernameRegex);

    if (validUsername) {
      admin.database().ref('/nicks/'+usernameLow).once('value').then((snapshot) => {
        currentUserName = snapshot.val();
        if (currentUserName) {
          return validUsername = false;
        } else {
          return validUsername = true;
        }
      }).catch((error) => {
        console.log(error);
      });
    } else {
      validUsername = false;
    }
      if ( validUsername && !currentUserName && data.gender && data.year < 2030 || 1900 ) {
        admin.auth().verifyIdToken(userIdToken)
          .then((decodedToken) => {
            var userId = decodedToken.uid;
            var profileData = {
                registeryDate: time,
                lastCheck: time,
                username: data.username,
                fullname: data.fullname,
                gender: data.gender,
                year: data.year,
                country: data.country,
                social: "-",
                photos: "-",
                activePoll: "-",
                stats: {
                  credit: 20,
                  totalVs: 0,
                  totalSingle: 0,
                  wins: 0,
                  points: 0,
                  votesCast: 0,
                  votesRecieved: 0
                }
              };

              var nick = {
                uid: userId
              };

              var updates = {};

              updates['/users/' + userId ] = profileData;
              updates['/nicks/' + usernameLow ] = nick;

              return admin.database().ref().update(updates).then(() => {
                return res.status(200).send("SUCCESS");
              });
          }).catch((error) => {
            return res.status(401).send("Invalid ID Token");
        });
      } else {
        return res.status(400).send("Invalid User Specification(s)");

      }
    //return res.status(400).send("Unknown Error");
});



exports.endPolls = functions.https.onRequest((req, res) => {
  var data = req.body; 
  var time = Date.now();
  admin.database().ref("/active-polls/").orderByChild('endTime').endAt(time).once("value").then((snapshot) => {
    snapshot.forEach((childSnapshot) => {
      var uidSecond;
      var fullnameSecond;
      var nickSecond;
      var photoSecond;
      var votesFirst;
      var votesSecond;
      var seen;
      var polls;
      var pollsL;
      var pollCount;
      var voters;
      var uid = childSnapshot.val().uid;
      var pollType = childSnapshot.val().pollType;
      var pollDuration = childSnapshot.val().pollDuration;
      var fullname = childSnapshot.val().fullname;
      var nick = childSnapshot.val().nick;
      var year = childSnapshot.val().year;
      var photo = childSnapshot.val().photo;
      var votes = childSnapshot.val().results.votes;
      var point = childSnapshot.val().results.point;

      var photoTime = "-" + photo;
      var photoTimeF = parseInt(photoTime);

      admin.database().ref("/active-polls-seen/"+uid).once("value").then((snapshot) => {
       voters = snapshot.val();
        if (pollType === "single") {
          admin.database().ref("/users/"+uid+"/stats").once("value").then((snapshot) => {
            var data = snapshot.val();
            var votesFinal = votes + data.votesRecieved;
            var totalSingle = data.totalSingle + 1;
            var pointFinal = (data.points * data.totalSingle + point ) / totalSingle;

            admin.database().ref("/users/"+uid+"/stats").update({
              points : pointFinal,
              totalSingle: totalSingle,
              votesRecieved: votesFinal
            });

            admin.database().ref("/users/"+uid).update({
              activePoll: "-",
            });



            admin.database().ref("/users/"+uid+"/photos/"+photo).update({
              point: point,
              votes: votes,
              pollType: "single",
              voters: voters,
              time: photoTimeF
            });
            /*
            var newNotifyRef = admin.database().ref("/notifications/" + uid).push();
            var newNotifyKey = newNotifyRef.key;
            newNotifyRef.set({
              type: "normal",
              key: newNotifyKey,
              msgID: "pollEnded"
            });
            */
          });

        } else {
          uid = childSnapshot.val().uid;
          pollType = childSnapshot.val().pollType;
          pollDuration = childSnapshot.val().pollDuration;
          fullname = childSnapshot.val().fullname;
          nick = childSnapshot.val().nick;
          year = childSnapshot.val().year;
          photo = childSnapshot.val().photo;
          uidSecond = childSnapshot.val().uidSecond;
          fullnameSecond = childSnapshot.val().fullnameSecond;
          nickSecond = childSnapshot.val().nickSecond;
          yearSecond = childSnapshot.val().yearSecond;
          photoSecond = childSnapshot.val().photoSecond;
          votesFirst = childSnapshot.val().results.votesFirst;
          votesSecond = childSnapshot.val().results.votesSecond;
          var totalVotes = votesFirst + votesSecond;

          admin.database().ref("/users/"+uid+"/stats").once("value").then((snapshot) => {
            var data = snapshot.val();
            var votesFinal = votesFirst + data.votesRecieved;
            var votesTotal = votesFirst + votesSecond;
            var totalVs = data.totalVs + 1;
            var percent = Math.round(votesFirst / (votesFirst + votesSecond)*100);
            var percentText = Math.round(percent) + "%";
            var wins;

            if ( votesFirst > votesSecond || votesFirst == votesSecond) {
              wins = data.wins + 1;
            } else {
              wins = data.wins;
            }


            admin.database().ref("/users/"+uid+"/stats").update({
              totalVs: totalVs,
              wins: wins,
              votesRecieved: votesFinal
            });

            admin.database().ref("/users/"+uid).update({
              activePoll: "-"
            });

            admin.database().ref("/users/"+uid+"/photos/"+photo).update({
              point: percentText,
              votes: votesTotal,
              pollType: "vs",
              opponentPhoto: photoSecond,
              opponentUid: uidSecond,
              opponentNick: nickSecond,
              voters: voters,
              time: photoTimeF
            });
            /*
            var newNotifyRef = admin.database().ref("/notifications/" + uid).push();
            var newNotifyKey = newNotifyRef.key;
            newNotifyRef.set({
              type: "normal",
              key: newNotifyKey,
              msgID: "pollEnded"
            }); */
          });

          admin.database().ref("/users/"+uidSecond+"/stats").once("value").then((snapshot) => {
            var data = snapshot.val();
            var votesFinal = votesSecond + data.votesRecieved;
            var votesTotal = votesFirst + votesSecond;
            var totalVs = data.totalVs + 1;
            var percent = Math.round(votesSecond / (votesFirst + votesSecond)* 100);
            var percentText = Math.round(percent) + "%";
            var wins;

            if ( votesSecond > votesFirst || votesFirst === votesSecond) {
              wins = data.wins + 1;
            } else {
              wins = data.wins;
            }

            admin.database().ref("/users/"+uidSecond+"/stats").update({
              totalVs: totalVs,
              wins: wins,
              votesRecieved: votesFinal
            });

            admin.database().ref("/users/"+uidSecond).update({
              activePoll: "-"
            });

            admin.database().ref("/users/"+uidSecond+"/photos/"+photoSecond).update({
              point: percentText,
              votes: votesTotal,
              pollType: "vs",
              opponentPhoto: photo,
              opponentUid: uid,
              opponentNick: nick,
              voters: voters,
              time: photoTimeF
            });

           

          });
        }
        admin.database().ref("/active-polls/"+uid).remove();
        admin.database().ref("/active-polls-seen/"+uid).remove();
      });
    });
    return res.status(200).send("SUCCESS");
  }).catch(error => {
    
  });
  return res.status(200).send("SUCCESS");
});



exports.choiceOne = functions.https.onRequest((req, res) => {
    var data = req.body;
    var pollID = data.pollID;
    var userIdToken = data.userIdToken;
    var votesFirst;
    var votesSecond;
    var previousVote;
    admin.auth().verifyIdToken(userIdToken)
    .then((decodedToken) => {
    var userId = decodedToken.uid;
    admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then((snapshot) => {
      var value = snapshot.val();
      if (value) {
        if ( value.rate === "second" ) {

          admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
            if ( data !== null ) {
              votesFirst = data.votesFirst;
              votesSecond = data.votesSecond;

              finalFirst = votesFirst + 1;
              finalSecond = votesSecond - 1;

              res.json({finalFirst: finalFirst, finalSecond: finalSecond});
              return { votesFirst: finalFirst, votesSecond: finalSecond };
            } else {
              return 0;
            }
          }, (error, committed, snapshot) => {
            if (error) {
              return res.status(400).send("Invalid User Specification(s)"+ error );
            } else if (!committed) {
              return res.status(400).send('We aborted the transaction (already exists).');
            } else {
              time = Date.now();
              console.log(snapshot);
              var voteTime = "-"+time;
              var voteTimeP = parseInt(voteTime);
              admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                time: voteTimeP,
                rate: "first",
                uid: userId
              });

              admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
                var nick = snapshot.val().nick;
                var uid = snapshot.val().uid;
                var photo = snapshot.val().photo;
                var nickSecond = snapshot.val().nickSecond;
                var uidSecond = snapshot.val().uidSecond;
                var photoSecond = snapshot.val().photoSecond;
                var voteTime = "-"+time;
                var voteTimeP = parseInt(voteTime);

                admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                  time: voteTimeP,
                  rate: "first",
                  pollType: "vs",
                  uid: uid,
                  nick: nick,
                  photo: photo,
                  uidSecond: uidSecond,
                  nickSecond: nickSecond,
                  photoSecond: photoSecond
                });

              });
            }
          }, true);
        }
      } else {
        admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
          if ( data !== null ) {
            votesFirst = data.votesFirst;
            votesSecond = data.votesSecond;

            finalFirst = votesFirst + 1;
            finalSecond = votesSecond ;

            res.json({finalFirst: finalFirst, finalSecond: finalSecond});
            return { votesFirst: finalFirst, votesSecond: finalSecond };
          } else {
            return 0;
          }
        }, (error, committed, snapshot) => {
          if (error) {
              return res.status(400).send("Invalid User Specification(s)"+ error );
            } else if (!committed) {
              return res.status(400).send('We aborted the transaction (already exists).');
            } else {
            time = Date.now();

            var voteTime = "-"+time;
            var voteTimeP = parseInt(voteTime);

            admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
              time: voteTimeP,
              rate: "first",
              uid: userId
            });

            admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
              var nick = snapshot.val().nick;
              var uid = snapshot.val().uid;
              var photo = snapshot.val().photo;
              var nickSecond = snapshot.val().nickSecond;
              var uidSecond = snapshot.val().uidSecond;
              var photoSecond = snapshot.val().photoSecond;
              var voteTime = "-"+time;
              var voteTimeP = parseInt(voteTime);
              admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                time: voteTimeP,
                rate: "first",
                pollType: "vs",
                uid: uid,
                nick: nick,
                photo: photo,
                uidSecond: uidSecond,
                nickSecond: nickSecond,
                photoSecond: photoSecond
              });
            });

            admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
              if ( votesCast !== null ) {
                return votesCast + 1 ;
              } else {
                return 0;
              }
            });
          }
        }, true);
      }
    });
  });
});

exports.choiceTwo = functions.https.onRequest((req, res) => {
      var data = req.body;
          var pollID = data.pollID;
          var userIdToken = data.userIdToken;
          var votesFirst;
          var votesSecond;
          var previousVote;
          admin.auth().verifyIdToken(userIdToken)
          .then((decodedToken) => {
          var userId = decodedToken.uid;
          admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then( (snapshot) => {
            var value = snapshot.val();
            if (value) {
              if ( value.rate === "first" ) {
                admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                  if ( data !== null ) {
                    votesFirst = data.votesFirst;
                    votesSecond = data.votesSecond;

                    finalFirst = votesFirst - 1;
                    finalSecond = votesSecond + 1;
                    res.json({finalFirst: finalFirst, finalSecond: finalSecond});
                    return { votesFirst: finalFirst, votesSecond: finalSecond };
                  } else {
                    return 0;
                  }
                }, (error, committed, snapshot) => {
                  if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                    time = Date.now();

                    var voteTime = "-"+time;
                    var voteTimeP = parseInt(voteTime);

                    admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                      time: voteTimeP,
                      rate: "second",
                      uid: userId
                    });

                    admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
                      var nick = snapshot.val().nick;
                      var uid = snapshot.val().uid;
                      var photo = snapshot.val().photo;
                      var nickSecond = snapshot.val().nickSecond;
                      var uidSecond = snapshot.val().uidSecond;
                      var photoSecond = snapshot.val().photoSecond;

                      var voteTime = "-"+time;
                      var voteTimeP = parseInt(voteTime);
                      admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                                    time: voteTimeP,
                          rate: "second",
                          pollType: "vs",
                          uid: uid,
                          nick: nick,
                          photo: photo,
                          uidSecond: uidSecond,
                          nickSecond: nickSecond,
                          photoSecond: photoSecond
                        });
            });
                  }
                }, true);
              }
            } else {
              admin.database().ref("/active-polls/"+pollID+"/results/").transaction((data) => {
                if ( data !== null ) {
                  votesFirst = data.votesFirst;
                  votesSecond = data.votesSecond;

                  finalFirst = votesFirst;
                  finalSecond = votesSecond + 1 ;
                  res.json({finalFirst: finalFirst, finalSecond: finalSecond});
                  
                  return { votesFirst: finalFirst, votesSecond: finalSecond };
                } else {
                  return 0;
                }
              }, (error, committed, snapshot) => {
                if (error) {
                    return res.status(400).send("Invalid User Specification(s)"+ error );
                  } else if (!committed) {
                    return res.status(400).send('We aborted the transaction (already exists).');
                  } else {
                  time = Date.now();

                  var voteTime = "-"+time;
                  var voteTimeP = parseInt(voteTime);

                  admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                    time: voteTimeP,
                    rate: "second",
                    uid: userId
                  });
         admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
            var nick = snapshot.val().nick;
            var uid = snapshot.val().uid;
            var photo = snapshot.val().photo;
            var nickSecond = snapshot.val().nickSecond;
            var uidSecond = snapshot.val().uidSecond;
            var photoSecond = snapshot.val().photoSecond;

            var voteTime = "-"+time;
            var voteTimeP = parseInt(voteTime);

            admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                          time: voteTimeP,
                          rate: "second",
                          pollType: "vs",
                          uid: uid,
                          nick: nick,
                          photo: photo,
                          uidSecond: uidSecond,
                          nickSecond: nickSecond,
                          photoSecond: photoSecond
                        });
            });
                  admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
                    if ( votesCast !== null ) {
                      return votesCast + 1 ;
                    } else {
                      return 0;
                    }
                  });
                }
              }, true);
            }
          });
         });
});

exports.rate = functions.https.onRequest((req, res) => {
  var data = req.body;
  var rate = data.rate;   
  var userIdToken = data.userIdToken;
  var pollID = data.pollID;
  var point;
  var votes;
  var finalPoint;
  var pointChange;
              
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    admin.database().ref("/active-polls-seen/"+ pollID + "/"+ userId ).once("value").then( (snapshot) => {
      var value = snapshot.val();
      if (value) {
          admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
            if ( data !== null ) {
              point = data.point;
              votes = data.votes;

              pointChange = (rate - value.rate ) / votes;
              finalPoint = point + pointChange;
              res.json({point: finalPoint});
              return { point: finalPoint, votes: votes };
            } else {
              return 0;
            }

          }, (error, committed, snapshot) => {
            if (error) {
            return res.status(400).send("Invalid User Specification(s)"+ error );
          } else if (!committed) {
            return res.status(400).send('We aborted the transaction (already exists).');
          } else {
              time = Date.now();
              var voteTime = "-"+time;
              var voteTimeP = parseInt(voteTime);
              admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                time: voteTimeP,
                rate: rate,
                uid: userId
              });

              admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
                var nick = snapshot.val().nick;
                var photo = snapshot.val().photo;
                var voteTime = "-"+time;
                var voteTimeP = parseInt(voteTime);

                admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                    time: voteTimeP,
                    rate: rate,
                    pollType: "single",
                    uid: pollID,
                    nick: nick,
                    photo: photo,
                });
              });

                }
              }, true);

          } else {
              time = Date.now();
              admin.database().ref("/active-polls/"+pollID+"/results/").transaction( (data) => {
                if ( data !== null ) {
                  point = data.point;
                  votes = data.votes;

                  finalPoint = (point * votes + parseInt(rate))/(votes + 1 );
                  res.json({point: finalPoint});
                  return { point: finalPoint, votes: votes +1 };
                } else {
                  return 0;
                }

              }, (error, committed, snapshot) => {
                if (error) {
                return res.status(400).send("Invalid User Specification(s)"+ error );
              } else if (!committed) {
                return res.status(400).send('We aborted the transaction (already exists).');
              } else {
                  var voteTime = "-"+time;
                  var voteTimeP = parseInt(voteTime);
                  var finalPoint = rate;
                  admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
                   time: voteTimeP,
                    rate: rate,
                    uid: userId,
                  });

                  admin.database().ref("/active-polls/"+pollID).once("value").then((snapshot) => {
                    var nick = snapshot.val().nick;
                    var photo = snapshot.val().photo;
                    var voteTime = "-"+time;
                    var voteTimeP = parseInt(voteTime);

                    admin.database().ref("/previous-votes/"+userId+"/"+pollID).update({
                        time: voteTimeP,
                        rate: rate,
                        pollType: "single",
                        uid: pollID,
                        nick: nick,
                        photo: photo,
                     });
                  });

                  
                  admin.database().ref("/users/"+userId+"/stats/votesCast").transaction( (votesCast) => {
                    if ( votesCast !== null ) {
                      res.json({point: finalPoint});
                      return votesCast + 1 ;
                    } else {
                      return 0;
                    }
                  });

                }
              }, true);
          }
    });

  });
});




exports.nick = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var newUsername = data.newUsername;
  var usernameRegex = /^[a-zA-Z0-9/_]{2,15}$/;
  var newUsernameLow = newUsername.toLowerCase();
  var validUsername = newUsername.match(usernameRegex);
  if (validUsername) {
    admin.database().ref('/nicks/'+newUsernameLow).once('value').then((snapshot) => {
      var currentUserName = snapshot.val();
      if ( currentUserName ) {
       return res.status(400).send("Username already exists.");
      } else {
        admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
          var userId = decodedToken.uid;
          admin.database().ref('/users/'+userId).once('value').then((snapshot) => {
            var oldUsername = snapshot.val().username;
            var oldUsernameLow = oldUsername.toLowerCase();
            var activePoll = snapshot.val().activePoll;
            admin.database().ref("nicks/" + oldUsernameLow).remove();

            var nick = {
              uid: userId
            };

            var updates = {};

            updates['/users/' + userId + '/username' ] = newUsername;
            updates['/nicks/' + newUsernameLow ] = nick;

            if (activePoll !== "-") {
              admin.database().ref('/pending-polls/'+activePoll).once('value').then((snapshot) => {
                try {
                  var pollType = snapshot.val().type;
                  if(pollType){
                    if (pollType == "single") {
                      admin.database().ref("/pending-polls/" + activePoll).update({
                        username: newUsername
                      });
                    } else {
                      var pollUID = snapshot.val().uid;
                      if (pollUID == userId ) {
                        admin.database().ref("/pending-polls/" + activePoll).update({
                          username: newUsername
                        });
                      } else {
                        admin.database().ref("/pending-polls/" + activePoll).update({
                          usernameSecond: newUsername
                        });
                      }
                    }
                  }

                } catch (e){
                  admin.database().ref('/active-polls/'+activePoll).once('value').then((snapshot) => {
                    try {
                      var pollType = snapshot.val().pollType;
                      if (pollType){
                        if (pollType === "single") {
                          admin.database().ref("/active-polls/" + activePoll).update({
                            nick: newUsername
                          });
                        } else {
                          var pollUID = snapshot.val().uid;
                          if (pollUID === userId ) {
                            admin.database().ref("/active-polls/" + activePoll).update({
                              nick: newUsername
                            });
                          } else {
                            admin.database().ref("/active-polls/" + activePoll).update({
                              nickSecond: newUsername
                            });
                          }
                        }
                      }
                    } catch (e) {
                      return res.status(400).send(e);
                    }
                  });
                }
              });
            }

            admin.database().ref().update(updates).then( () => {
              return res.status(200).send("Saved");
            });
          
          });
        });
      }
    });
  } else {
    return res.status(400).send("Username already exists.");
  }

});


exports.upload2 = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var time =  Date.now();
  var ref = "/pending-polls/";
  var pollType = data.pollType;
  var toWhom = data.toWhom;
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    var pollDuration = data.pollDuration;

    if (pollType === "vs" && toWhom === "determined") {
       ref = "/pending-requests/";
     }
    
    admin.database().ref('/users/'+userId).once('value').then((snapshot) => {
       var username = snapshot.val().username;
       var fullname = snapshot.val().fullname;
       var birthyear = snapshot.val().year;
       var gender = snapshot.val().gender;
       var now = data.now;

       admin.database().ref( ref + userId).update({
         uid : userId,
         username : username,
         fullname : fullname,
         year : birthyear,
         gender : gender,
         pollDuration: pollDuration,
         type: pollType,
         toWhom: toWhom,
         photo: now,
         lastCheck: now
       }).then( () => {
         if ( pollType == "vs") {
           var cUsername;
           if ( toWhom == "determined" ) {
             var cUsernameF = data.cUsernameF;
             var cUsername = cUsernameF.toLowerCase();
             var cUID;
             admin.database().ref("nicks/" + cUsername).once("value").then( (snapshot) => {
               cUID = snapshot.val().uid;
               admin.database().ref(ref + userId).update({
                 toWhom: toWhom,
                 usernameSecond: cUsername,
                 uidSecond: cUID
               });

               admin.database().ref("/users/"+cUID).update({
                 activePoll: userId
               });

               var notifyRef = admin.database().ref("notifications/" + cUID + "/" + cUID).update({
                 key: cUID,
                 type: "challenge",
                 username: username,
                 uid: userId
               });
             });
           } else {
             var date = new Date();
             var yearNow = date.getFullYear();
             var age = yearNow - birthyear;
             var ageRange;
             if ( age < 16 ) {
               ageRange = "child";
             } else if ( age < 25 ) {
               ageRange = "young";
             } else if ( age < 40) {
               ageRange = "middle";
             } else if (age < 60) {
               ageRange = "old";
             } else {
               ageRange = "older";
             }
             admin.database().ref(ref + userId).update({
               usernameSecond: "-",
               ageRange: ageRange
             });

             admin.database().ref("/pending-vs/" + pollDuration + "/" + gender + "/" + ageRange + "/" + userId).update({
               uid : userId
             });

           }

         }

         admin.database().ref("/users/" + userId).update({
           activePoll: userId
         });
         return res.status(200).send("SUCCESS");
       });
     });
  });
});

exports.upload3 = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var time =  Date.now();
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    admin.database().ref('/users/'+userId).once('value').then((snapshot) => {
      var username = snapshot.val().username;
      var fullname = snapshot.val().fullname;
      var birthyear = snapshot.val().year;
      var gender = snapshot.val().gender;
      var now = data.now;

      admin.database().ref('/notifications/'+userId+"/"+userId).once('value').then((snapshot) => {
        var notifyKey = snapshot.val().key;
        var notifyType = snapshot.val().type;
        var cUID = snapshot.val().uid;
        admin.database().ref('/users/'+userId).once('value').then((snapshot) => {
          var username = snapshot.val().username;
          var fullname = snapshot.val().fullname;
          var year = snapshot.val().year;
          var gender = snapshot.val().gender;



          if ( notifyType === "challenge" ) {
            admin.database().ref('/pending-requests/'+cUID).once('value').then((snapshot) => {
              var fullnameFirst = snapshot.val().fullname;
              var genderFirst = snapshot.val().gender;
              var yearFirst = snapshot.val().year;
              var usernameFirst = snapshot.val().username;
              var photoFirst = snapshot.val().photo;
              var uidFirst = snapshot.val().uid;
              var pollDuration = snapshot.val().pollDuration;
              var toWhom = snapshot.val().toWhom;
              var type = snapshot.val().type;

              admin.database().ref("/pending-polls/" + cUID).update({
                fullname: fullnameFirst,
                gender: genderFirst,
                year: yearFirst,
                username: usernameFirst,
                uid: uidFirst,
                photo: photoFirst,
                pollDuration: pollDuration,
                toWhom: toWhom,
                type: type,
                uidSecond : userId,
                usernameSecond : username,
                fullnameSecond : fullname,
                yearSecond : birthyear,
                genderSecond : gender,
                photoSecond: now
              }).then( () => {
                admin.database().ref('/notifications/'+userId+"/"+userId).remove();
                admin.database().ref('/pending-requests/'+cUID+"/").remove();
                admin.database().ref("/users/" + userId).update({
                  activePoll: cUID
                });

              });
              return res.status(200).send("SUCCESS");

            });
          }
        });
      });
    });
  });
});


exports.reject = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var cUID = data.cUID;
  var notifyKey = data.notifyKey;
  var time =  Date.now();
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    admin.database().ref("/notifications/"+userId+"/"+notifyKey).remove();
      admin.database().ref("/pending-requests/"+cUID).remove();
      admin.database().ref("/users/" + userId).update({
        activePoll: "-"
      });
      admin.database().ref("/users/" + cUID).update({
        activePoll: "-"
      });
      var newNotifyRef = admin.database().ref("/notifications/" + cUID).push();
      var newNotifyKey = newNotifyRef.key;
      newNotifyRef.set({
        type: "challengeRejected",
        key: newNotifyKey
      });
      return res.status(200).send("SUCCESS");
  });
});


exports.cancelRequest = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var cUID = data.cUID;
  var time =  Date.now();
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    console.log(cUID);
    admin.database().ref("pending-requests/"+userId).remove();
    admin.database().ref("notifications/"+cUID+"/"+cUID).remove();
    admin.database().ref("users/" + userId).update({
      activePoll: "-"
    }).then( () => {
      admin.database().ref("users/" + cUID).update({
        activePoll: "-"
      });
      return res.status(200).send("SUCCESS");
    });
    
  });
});

exports.cancelPoll = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
    admin.database().ref("/pending-polls/"+userId).remove();
      admin.database().ref("users/" + userId).update({
        activePoll: "-"
      }).then( () => {
        admin.database().ref('/pending-polls/'+userId).once('value').then((snapshot) => {
          var toWhom = snapshot.val().toWhom;
          if (toWhom === "determined") {
            var cUID = snapshot.val().uidSecond;
            admin.database().ref("users/" + cUID).update({
              activePoll: "-"
            });
                admin.database().ref("notifications/"+cUID+"/"+cUID).remove();

          } else {
            var ageRange = snapshot.val().ageRange;
            var pollDuration = snapshot.val().pollDuration;
            var gender = snapshot.val().gender;
            admin.database().ref("/pending-vs/" + pollDuration + "/" + gender + "/"+ ageRange + "/" + userId ).remove();
          }
        });
      });
      return res.status(200).send("SUCCESS");
  });
});


exports.report = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var pollID = data.pollID;
  var time =  Date.now();
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    var userId = decodedToken.uid;
     admin.database().ref("/reports/"+pollID).update({
        pollID : pollID,
        reporter: userId
      });
     admin.database().ref("/active-polls-seen/"+pollID+"/"+userId).update({
              time: time,
              uid: userId
            });
     return res.status(200).send("SUCCESS");
  });
});


exports.acceptPoll = functions.https.onRequest((req, res) => {
  var data = req.body;
  var userIdToken = data.userIdToken;
  var time =  Date.now();
  var pollType ;
  var pollDuration;
  var toWhom;
  var uid ;
  var fullname;
  var username;
  var year;
  var gender;
  var photo;
  var lastCheck;
  var uidSecond;
  var fullnameSecond;
  var usernameSecond;
  var yearSecond;
  var genderSecond;
  var photoSecond;
  var ageRange;

function acceptPoll() {

var time = Date.now();
  var now;
  var endTime;
  var newNotifyKey;
  var newNotifyRef;
  admin.database().ref("/pending-polls/"+uid).update({
    lastCheck: time
  }).then( () => {
    admin.database().ref('/pending-polls/'+uid).once('value').then((snapshot) => {
      now = snapshot.val().lastCheck;
      if (pollDuration === "5m") {
        endTime = now + 300000;
      } else if (pollDuration === "1h") {
        endTime = now + 3600000;
      } else if (pollDuration === "12h") {
        endTime = now + 43200000;
      } else {
        endTime = now + 86400000;
      }

      admin.database().ref("/active-polls/"+uid).update({
        pollType : pollType,
        pollDuration : pollDuration,
        uid : uid,
        fullname : fullname,
        nick : username,
        year : year,
        gender : gender,
        photo : photo,
        endTime : endTime,
        results: "-"
      });

      admin.database().ref("/active-polls-seen/"+uid+"/"+uid).update({
        time: time
      });




      if ( pollType === "vs" ) {
        admin.database().ref("/active-polls/"+uid).update({
          uidSecond : uidSecond,
          fullnameSecond : fullnameSecond,
          nickSecond : usernameSecond,
          yearSecond : yearSecond,
          genderSecond : genderSecond,
          photoSecond : photoSecond,
        });

        admin.database().ref("/active-polls-seen/"+uid+"/"+uidSecond).update({
          time: time
        });

        admin.database().ref("/active-polls/"+uid+"/results").update({
          votesFirst: 0,
          votesSecond: 0
        });
        admin.database().ref("/pending-polls/"+uidSecond).remove();
        admin.database().ref("/users/"+uidSecond).update({
          activePoll: uid
        });

        admin.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange +"/"+ uidSecond).remove();
        /*
        newNotifyRef = admin.database().ref("/notifications/" + uid).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });

        newNotifyRef = admin.database().ref("/notifications/" + uidSecond).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });
        */
      } else {
        admin.database().ref("/active-polls/"+uid+"/results").update({
          point: 0,
          votes: 0
        });
      /*
        newNotifyRef = admin.database().ref("/notifications/" + uid).push();
        newNotifyKey = newNotifyRef.key;
        newNotifyRef.set({
          type: "normal",
          key: newNotifyKey,
          msgID: "pollPublished"
        });
        */

      }

      admin.database().ref("/pending-polls/"+uid).remove();
      admin.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange +"/"+ uid).remove();
      admin.database().ref("/users/"+uid).update({
        activePoll: uid
      });


    });
  });
}
  admin.auth().verifyIdToken(userIdToken).then((decodedToken) => {
    admin.database().ref("/pending-polls/").orderByChild('lastCheck').once("value").then((snapshot) => {
      snapshot.forEach((childSnapshot) => {
        pollType = childSnapshot.val().type;
        pollDuration = childSnapshot.val().pollDuration;
        uid = childSnapshot.val().uid;
        fullname = childSnapshot.val().fullname;
        username = childSnapshot.val().username;
        year = childSnapshot.val().year;
        gender = childSnapshot.val().gender;
        photo = childSnapshot.val().photo;
        lastCheck = childSnapshot.val().lastCheck;
        toWhom = childSnapshot.val().toWhom;

        //AUTO ACCEPT


        if ( pollType === "vs") {


          if (toWhom === "determined") {
            uidSecond = childSnapshot.val().uidSecond;
            fullnameSecond = childSnapshot.val().fullnameSecond;
            usernameSecond = childSnapshot.val().usernameSecond;
            yearSecond = childSnapshot.val().yearSecond;
            genderSecond = childSnapshot.val().genderSecond;
            photoSecond = childSnapshot.val().photoSecond;
            acceptPoll();
          } else {
              ageRange = childSnapshot.val().ageRange;
              admin.database().ref("/pending-vs/"  + pollDuration + "/" + gender + "/"+ ageRange).limitToFirst(2).once("value").then((snapshot) => {
                var pending = snapshot.val();
                var pendingL = Object.keys(pending).length;
                if (pendingL !== 1) {
                  snapshot.forEach((childSnapshot) => {
                    uidSecond = childSnapshot.val().uid;
                    if ( childSnapshot.val().uid !== uid ) {

                      uidSecond = childSnapshot.val().uid;
                      admin.database().ref('/pending-polls/'+uidSecond).once('value').then((snapshot) => {
                        fullnameSecond = snapshot.val().fullname;
                        usernameSecond = snapshot.val().username;
                        yearSecond = snapshot.val().year;
                        genderSecond = snapshot.val().gender;
                        photoSecond = snapshot.val().photo;
                        uidSecond = childSnapshot.val().uid;
                          acceptPoll();
                        
                      });
                    }

                  });
                } else {
                  var time = Date.now();
                  admin.database().ref("/pending-polls/"+uid).update({
                    lastCheck: time
                  });


                }
              });



          }
        } else {
            acceptPoll();
        }
      });
    });


     return res.status(200).send("SUCCESS");
  });
});